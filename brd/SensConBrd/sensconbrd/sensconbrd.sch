EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 6E58D2DE
P 2650 1650
F 0 "J?" H 2700 2067 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2700 1976 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 2650 1650 50  0001 C CNN
F 3 "~" H 2650 1650 50  0001 C CNN
	1    2650 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 6E58DC78
P 5000 1650
F 0 "J?" H 5050 2067 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5050 1976 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 5000 1650 50  0001 C CNN
F 3 "~" H 5000 1650 50  0001 C CNN
	1    5000 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1450 2950 1450
Connection ~ 2950 1450
Wire Wire Line
	4800 1450 5300 1450
Connection ~ 4800 1450
Wire Wire Line
	2950 1550 2950 1450
Wire Wire Line
	2950 1450 4800 1450
Wire Wire Line
	4800 1550 4800 1450
Wire Wire Line
	5300 1550 5300 1450
Connection ~ 5300 1450
Wire Wire Line
	2450 1550 2450 1450
Connection ~ 2450 1450
Wire Wire Line
	2450 1750 2450 1850
Wire Wire Line
	2950 1850 2950 1750
Wire Wire Line
	2950 1750 4800 1750
Connection ~ 2950 1750
Wire Wire Line
	4800 1750 4800 1850
Connection ~ 4800 1750
Wire Wire Line
	2950 1750 2450 1750
Connection ~ 2450 1750
Wire Wire Line
	5300 1850 5300 1750
Wire Wire Line
	5300 1750 4800 1750
Connection ~ 5300 1750
Wire Wire Line
	5300 1650 5300 1600
Wire Wire Line
	5300 1600 2950 1600
Wire Wire Line
	2950 1600 2950 1650
Wire Wire Line
	4800 1650 4800 1700
Wire Wire Line
	4800 1700 2450 1700
Wire Wire Line
	2450 1700 2450 1650
$EndSCHEMATC
