EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J?
U 1 1 6E59A9F7
P 3250 3850
F 0 "J?" H 3300 4367 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 3300 4276 50  0000 C CNN
F 2 "" H 3250 3850 50  0001 C CNN
F 3 "~" H 3250 3850 50  0001 C CNN
	1    3250 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J?
U 1 1 6E59B935
P 6800 3850
F 0 "J?" H 6850 4367 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 6850 4276 50  0000 C CNN
F 2 "" H 6800 3850 50  0001 C CNN
F 3 "~" H 6800 3850 50  0001 C CNN
	1    6800 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3850 3550 3900
Wire Wire Line
	7200 3850 7100 3850
Wire Wire Line
	2950 3800 2950 3850
Wire Wire Line
	2950 3850 3050 3850
Wire Wire Line
	3550 3750 3550 3650
Connection ~ 3550 3550
Wire Wire Line
	3550 3550 3550 3200
Connection ~ 3550 3650
Wire Wire Line
	3550 3650 3550 3550
Wire Wire Line
	3050 3750 3050 3650
Connection ~ 3050 3550
Wire Wire Line
	3050 3550 3050 3200
Connection ~ 3050 3650
Wire Wire Line
	3050 3650 3050 3550
Wire Wire Line
	3050 3950 3050 4050
Connection ~ 3050 4050
Wire Wire Line
	3050 4050 3050 4150
Connection ~ 3050 4150
Wire Wire Line
	3050 4150 3050 4400
Wire Wire Line
	3550 3950 3550 4050
Connection ~ 3550 4050
Wire Wire Line
	3550 4050 3550 4150
Connection ~ 3550 4150
Wire Wire Line
	3550 4150 3550 4400
$Comp
L power:GND #PWR?
U 1 1 6E5A568F
P 3550 4400
F 0 "#PWR?" H 3550 4150 50  0001 C CNN
F 1 "GND" H 3555 4227 50  0000 C CNN
F 2 "" H 3550 4400 50  0001 C CNN
F 3 "" H 3550 4400 50  0001 C CNN
	1    3550 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5A5B89
P 3050 4400
F 0 "#PWR?" H 3050 4150 50  0001 C CNN
F 1 "GND" H 3055 4227 50  0000 C CNN
F 2 "" H 3050 4400 50  0001 C CNN
F 3 "" H 3050 4400 50  0001 C CNN
	1    3050 4400
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5A5EB1
P 3550 3200
F 0 "#PWR?" H 3550 3050 50  0001 C CNN
F 1 "+24V" H 3565 3373 50  0000 C CNN
F 2 "" H 3550 3200 50  0001 C CNN
F 3 "" H 3550 3200 50  0001 C CNN
	1    3550 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5A6291
P 3050 3200
F 0 "#PWR?" H 3050 3050 50  0001 C CNN
F 1 "+24V" H 3065 3373 50  0000 C CNN
F 2 "" H 3050 3200 50  0001 C CNN
F 3 "" H 3050 3200 50  0001 C CNN
	1    3050 3200
	1    0    0    -1  
$EndComp
Text Label 3850 3800 0    50   ~ 0
canh
Text Label 3850 3900 0    50   ~ 0
canl
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J?
U 1 1 6E5A6F7D
P 4900 3850
F 0 "J?" H 4950 4367 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 4950 4276 50  0000 C CNN
F 2 "" H 4900 3850 50  0001 C CNN
F 3 "~" H 4900 3850 50  0001 C CNN
	1    4900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3800 4700 3850
Wire Wire Line
	5200 3900 5200 3850
Connection ~ 4700 3800
Connection ~ 5200 3900
Wire Wire Line
	6600 3800 6600 3850
Wire Wire Line
	7200 3900 7200 3850
Wire Wire Line
	4700 3950 4700 4050
Connection ~ 4700 4050
Wire Wire Line
	4700 4050 4700 4150
Connection ~ 4700 4150
Wire Wire Line
	4700 4150 4700 4400
Wire Wire Line
	5200 3950 5200 4050
Connection ~ 5200 4050
Wire Wire Line
	5200 4050 5200 4150
Connection ~ 5200 4150
Wire Wire Line
	5200 4150 5200 4400
Wire Wire Line
	6600 3950 6600 4050
Connection ~ 6600 4050
Wire Wire Line
	6600 4050 6600 4150
Connection ~ 6600 4150
Wire Wire Line
	6600 4150 6600 4400
Wire Wire Line
	7100 3950 7100 4050
Connection ~ 7100 4050
Wire Wire Line
	7100 4050 7100 4150
Connection ~ 7100 4150
Wire Wire Line
	7100 4150 7100 4400
Wire Wire Line
	4700 3750 4700 3650
Connection ~ 4700 3550
Wire Wire Line
	4700 3550 4700 3200
Connection ~ 4700 3650
Wire Wire Line
	4700 3650 4700 3550
Wire Wire Line
	5200 3750 5200 3650
Connection ~ 5200 3550
Wire Wire Line
	5200 3550 5200 3200
Connection ~ 5200 3650
Wire Wire Line
	5200 3650 5200 3550
Wire Wire Line
	6600 3750 6600 3650
Connection ~ 6600 3550
Wire Wire Line
	6600 3550 6600 3200
Connection ~ 6600 3650
Wire Wire Line
	6600 3650 6600 3550
Wire Wire Line
	7100 3750 7100 3650
Connection ~ 7100 3550
Wire Wire Line
	7100 3550 7100 3200
Connection ~ 7100 3650
Wire Wire Line
	7100 3650 7100 3550
$Comp
L power:+24V #PWR?
U 1 1 6E5B5261
P 4700 3200
F 0 "#PWR?" H 4700 3050 50  0001 C CNN
F 1 "+24V" H 4715 3373 50  0000 C CNN
F 2 "" H 4700 3200 50  0001 C CNN
F 3 "" H 4700 3200 50  0001 C CNN
	1    4700 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5B56B3
P 5200 3200
F 0 "#PWR?" H 5200 3050 50  0001 C CNN
F 1 "+24V" H 5215 3373 50  0000 C CNN
F 2 "" H 5200 3200 50  0001 C CNN
F 3 "" H 5200 3200 50  0001 C CNN
	1    5200 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5B6183
P 6600 3200
F 0 "#PWR?" H 6600 3050 50  0001 C CNN
F 1 "+24V" H 6615 3373 50  0000 C CNN
F 2 "" H 6600 3200 50  0001 C CNN
F 3 "" H 6600 3200 50  0001 C CNN
	1    6600 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5B6625
P 7100 3200
F 0 "#PWR?" H 7100 3050 50  0001 C CNN
F 1 "+24V" H 7115 3373 50  0000 C CNN
F 2 "" H 7100 3200 50  0001 C CNN
F 3 "" H 7100 3200 50  0001 C CNN
	1    7100 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5B6A30
P 4700 4400
F 0 "#PWR?" H 4700 4150 50  0001 C CNN
F 1 "GND" H 4705 4227 50  0000 C CNN
F 2 "" H 4700 4400 50  0001 C CNN
F 3 "" H 4700 4400 50  0001 C CNN
	1    4700 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5B6D86
P 5200 4400
F 0 "#PWR?" H 5200 4150 50  0001 C CNN
F 1 "GND" H 5205 4227 50  0000 C CNN
F 2 "" H 5200 4400 50  0001 C CNN
F 3 "" H 5200 4400 50  0001 C CNN
	1    5200 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5B7677
P 6600 4400
F 0 "#PWR?" H 6600 4150 50  0001 C CNN
F 1 "GND" H 6605 4227 50  0000 C CNN
F 2 "" H 6600 4400 50  0001 C CNN
F 3 "" H 6600 4400 50  0001 C CNN
	1    6600 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5B7E36
P 7100 4400
F 0 "#PWR?" H 7100 4150 50  0001 C CNN
F 1 "GND" H 7105 4227 50  0000 C CNN
F 2 "" H 7100 4400 50  0001 C CNN
F 3 "" H 7100 4400 50  0001 C CNN
	1    7100 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 6E5C4915
P 3850 2750
F 0 "C?" H 3968 2796 50  0000 L CNN
F 1 "270uF" H 3968 2705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 3888 2600 50  0001 C CNN
F 3 "~" H 3850 2750 50  0001 C CNN
	1    3850 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5C5160
P 3850 2600
F 0 "#PWR?" H 3850 2450 50  0001 C CNN
F 1 "+24V" H 3865 2773 50  0000 C CNN
F 2 "" H 3850 2600 50  0001 C CNN
F 3 "" H 3850 2600 50  0001 C CNN
	1    3850 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5C557B
P 3850 2900
F 0 "#PWR?" H 3850 2650 50  0001 C CNN
F 1 "GND" H 3855 2727 50  0000 C CNN
F 2 "" H 3850 2900 50  0001 C CNN
F 3 "" H 3850 2900 50  0001 C CNN
	1    3850 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 6E5C5BF2
P 4300 2750
F 0 "C?" H 4418 2796 50  0000 L CNN
F 1 "270uF" H 4418 2705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 4338 2600 50  0001 C CNN
F 3 "~" H 4300 2750 50  0001 C CNN
	1    4300 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5C5BF8
P 4300 2600
F 0 "#PWR?" H 4300 2450 50  0001 C CNN
F 1 "+24V" H 4315 2773 50  0000 C CNN
F 2 "" H 4300 2600 50  0001 C CNN
F 3 "" H 4300 2600 50  0001 C CNN
	1    4300 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5C5BFE
P 4300 2900
F 0 "#PWR?" H 4300 2650 50  0001 C CNN
F 1 "GND" H 4305 2727 50  0000 C CNN
F 2 "" H 4300 2900 50  0001 C CNN
F 3 "" H 4300 2900 50  0001 C CNN
	1    4300 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 6E5C769E
P 5900 2800
F 0 "C?" H 6018 2846 50  0000 L CNN
F 1 "270uF" H 6018 2755 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 5938 2650 50  0001 C CNN
F 3 "~" H 5900 2800 50  0001 C CNN
	1    5900 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5C76A4
P 5900 2650
F 0 "#PWR?" H 5900 2500 50  0001 C CNN
F 1 "+24V" H 5915 2823 50  0000 C CNN
F 2 "" H 5900 2650 50  0001 C CNN
F 3 "" H 5900 2650 50  0001 C CNN
	1    5900 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5C76AA
P 5900 2950
F 0 "#PWR?" H 5900 2700 50  0001 C CNN
F 1 "GND" H 5905 2777 50  0000 C CNN
F 2 "" H 5900 2950 50  0001 C CNN
F 3 "" H 5900 2950 50  0001 C CNN
	1    5900 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 6E5C923D
P 6250 2800
F 0 "C?" H 6368 2846 50  0000 L CNN
F 1 "270uF" H 6368 2755 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 6288 2650 50  0001 C CNN
F 3 "~" H 6250 2800 50  0001 C CNN
	1    6250 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR?
U 1 1 6E5C9243
P 6250 2650
F 0 "#PWR?" H 6250 2500 50  0001 C CNN
F 1 "+24V" H 6265 2823 50  0000 C CNN
F 2 "" H 6250 2650 50  0001 C CNN
F 3 "" H 6250 2650 50  0001 C CNN
	1    6250 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6E5C9249
P 6250 2950
F 0 "#PWR?" H 6250 2700 50  0001 C CNN
F 1 "GND" H 6255 2777 50  0000 C CNN
F 2 "" H 6250 2950 50  0001 C CNN
F 3 "" H 6250 2950 50  0001 C CNN
	1    6250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3800 6600 3800
Wire Wire Line
	5200 3900 7200 3900
Wire Wire Line
	2950 3800 4700 3800
Wire Wire Line
	3550 3900 5200 3900
$EndSCHEMATC
